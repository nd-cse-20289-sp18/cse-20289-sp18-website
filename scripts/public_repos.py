#!/usr/bin/env python3

import requests

ACCESS_TOKEN = 'sembym2-TsfPwkCbwPrH'
HEADERS      = {'PRIVATE-TOKEN': ACCESS_TOKEN}
PROJECTS_URL = 'https://gitlab.com/api/v4/projects'
USER_ID      = 268318

def search_projects(query):
    url      = PROJECTS_URL
    results  = []
    projects = True
    page     = 1

    while projects:
        params   = {'page': page, 'per_page': 100, 'simple': True, 'search': query, 'membership': True, 'owned': False}
        response = requests.get(url, params=params, headers=HEADERS)
        projects = response.json()
        page     = page + 1

        for project in projects:
            yield project

if __name__ == '__main__':
    #for project in search_projects('cdt'):
    for project in search_projects('cse-20289-sp18-assignments'):
        if ('nd-cse' in project['web_url'] or
            'nd-cdt' in project['web_url'] or
            'pbui'   in project['web_url']):
            continue

        response     = requests.get('{}/{}'.format(PROJECTS_URL, project['id']), headers=HEADERS)
        project_data = response.json()
        if project_data['visibility'] != 'private':
            print(project['web_url'])
