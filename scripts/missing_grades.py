#!/usr/bin/env python3

import collections
import csv
import glob
import os
import yaml

# Constants

GRADES   = 'data/SP18_CSE_20289_01.csv'
TAS      = 'static/yaml/ta.yaml'
MAPPINGS = 'static/yaml/reading*.yaml'

# Grades

def load_grades(path):
    grades = {}
    for student in csv.DictReader(open(path)):
        data = {}
        for k, v in student.items():
            k = ''.join(k.lower().split()).split('(')[0]
            data[k] = v
        grades[data['studentid']] = data

    return grades

# Mappings

def load_mappings(pattern):
    mappings = {}
    for path in glob.glob(pattern):
        name  = os.path.basename(path).split('.')[0]
        index = int(name[-2:])
        data  = collections.defaultdict(list)
        for student, ta in yaml.load(open(path)):
            data[ta].append(student)
        
        mappings[name] = data
        mappings['homework{:02d}'.format(index)] = data

    return mappings

def find_missing(mappings, grades):
    missing = collections.defaultdict(dict)

    for assignment, mapping in mappings.items():
        for ta, students in mapping.items():
            missing[ta][assignment] = []
            for student in students:
                try:
                    if not grades[student][assignment]:
                        missing[ta][assignment].append(student)
                except KeyError:
                    pass

    return missing

# Main Execution

if __name__ == '__main__':
    grades   = load_grades(GRADES)
    mappings = load_mappings(MAPPINGS)
    missing  = find_missing(mappings, grades)

    for ta, assignments in sorted(missing.items()):
        print(ta)
        for assignment, students in sorted(assignments.items()):
            if students:
                print('{:>15}: {}'.format(assignment, ', '.join(students)))
        print()

